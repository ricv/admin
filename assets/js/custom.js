$(function() {
	$(".has-treeview>a").click(function(){
		var li = $(this).closest('li');
		var abierto = li.hasClass('active');
		if (abierto == true) {
			li.removeClass('active');
		}else{
			li.addClass('active');
		}
	})
	$(".sidebar").click(function(){
		if ($(this).hasClass('ver')) {
			verSidebar();
		}else{			
			ocultarSidebar();
		}
	})

	$(".side-bar>.background").click(function(e){
		ocultarSidebar()
	})

});

$(window).on('load',function(){
	validarSidebar();
	verSidebar();
});

$(window).on('resize',function(){
	validarSidebar()
});

function validarSidebar(){
	if ($(this).width()<= 768) {
		ocultarSidebar();
	}else{
		verSidebar();
	}
}

function verSidebar(){
	$(".side-bar").removeClass('ocultar');
	$(".contenido").removeClass('completo');
	$(".cabecera").removeClass('completo');
	$(".sidebar").removeClass('ver');
	$(".sidebar").addClass('ocultar');
	if ($(this).width()<= 768) {
		$(".side-bar").addClass('ver');
	}else{
		$(".side-bar").removeClass('ver');
	}
}

function ocultarSidebar(){
	$(".side-bar").addClass('ocultar');
	$(".contenido").addClass('completo');
	$(".cabecera").addClass('completo');
	$(".sidebar").removeClass('ocultar');
	$(".sidebar").addClass('ver');
	if ($(this).width()<= 768) {
		$(".side-bar").removeClass('ver');
	}else{
		$(".side-bar").addClass('ver');
	}
}